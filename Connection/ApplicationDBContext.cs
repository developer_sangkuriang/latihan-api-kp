﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using WebAPI_NETCore.Models;

namespace WebAPI_NETCore.Connection;

public partial class ApplicationDBContext : DbContext
{
    #region Call connection string on appsetting
    static IConfiguration AppSettingJSON = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build());
    static IConfiguration AppSettingJSONDevelopment = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.Development.json").Build());
    //public static string GetConnection = AppSettingJSON.GetSection("ConnectionStrings").GetValue<string>("DefaultConnection");
    public static string GetConnection = AppSettingJSON["ConnectionStrings:DefaultConnection"].ToString();
    public static string GetConnectionDevelopment = AppSettingJSONDevelopment["ConnectionStrings:DefaultConnection"].ToString();
    #endregion

    public ApplicationDBContext()
    {
    }

    public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options): base(options)
    {
    }

    public virtual DbSet<DaftarPemesan> DaftarPemesans { get; set; }

    public virtual DbSet<HistorySignIn> HistorySignIns { get; set; }

    public virtual DbSet<MasterEmployee> MasterEmployees { get; set; }

    public virtual DbSet<MasterMenu> MasterMenus { get; set; }

    public virtual DbSet<Pesanan> Pesanans { get; set; }

    public virtual DbSet<Transaksi> Transaksis { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => optionsBuilder.UseSqlServer(GetConnection);

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<DaftarPemesan>(entity =>
        {
            entity.HasKey(e => e.KodePemesan).HasName("PK_Pesanan");

            entity.ToTable("DaftarPemesan");

            entity.Property(e => e.KodePemesan)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.KodeMenu)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.NamaPemesan)
                .HasMaxLength(50)
                .IsUnicode(false);

            entity.HasOne(d => d.KodeMenuNavigation).WithMany(p => p.DaftarPemesans)
                .HasForeignKey(d => d.KodeMenu)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_MasterMenu_DaftarPesanan");
        });

        modelBuilder.Entity<HistorySignIn>(entity =>
        {
            entity.ToTable("HistorySignIn");

            entity.Property(e => e.LastLogged).HasColumnType("datetime");
            entity.Property(e => e.Nik).HasColumnName("NIK");

            entity.HasOne(d => d.NikNavigation).WithMany(p => p.HistorySignIns)
                .HasForeignKey(d => d.Nik)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_HistorySignIn_MasterEmployee");
        });

        modelBuilder.Entity<MasterEmployee>(entity =>
        {
            entity.HasKey(e => e.Nik);

            entity.ToTable("MasterEmployee");

            entity.Property(e => e.Nik)
                .ValueGeneratedNever()
                .HasColumnName("NIK");
            entity.Property(e => e.AlamatDomisili)
                .HasMaxLength(500)
                .IsUnicode(false);
            entity.Property(e => e.AlamatLengkap)
                .HasMaxLength(500)
                .IsUnicode(false);
            entity.Property(e => e.Email1)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Email2)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.HakAkses)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");
            entity.Property(e => e.Jabatan)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.JenisKelamin)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.NamaLengkap)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.NamaPanggilan)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.NomerTelepon)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.PasswordLogin)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.TanggalLahir).HasColumnType("datetime");
            entity.Property(e => e.TempatLahir)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.UsernameLogin)
                .HasMaxLength(50)
                .IsUnicode(false);
        });

        modelBuilder.Entity<MasterMenu>(entity =>
        {
            entity.HasKey(e => e.KodeMenu);

            entity.ToTable("MasterMenu");

            entity.Property(e => e.KodeMenu)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.HargaSatuan).HasColumnType("decimal(18, 0)");
            entity.Property(e => e.Id).ValueGeneratedOnAdd(); //.UseIdentityColumn();
            entity.Property(e => e.JenisMenu)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.NamaMenu)
                .HasMaxLength(50)
                .IsUnicode(false);
        });

        modelBuilder.Entity<Pesanan>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("Pesanan");

            entity.Property(e => e.HargaSatuan).HasColumnType("decimal(18, 0)");
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.KodeMenu)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.KodePemesan)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.KodeTransaksi)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Nik).HasColumnName("NIK");

            entity.HasOne(d => d.KodeMenuNavigation).WithMany()
                .HasForeignKey(d => d.KodeMenu)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Pesanan_MasterMenu");

            entity.HasOne(d => d.KodePemesanNavigation).WithMany()
                .HasForeignKey(d => d.KodePemesan)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Pesanan_DaftarPemesan");

            entity.HasOne(d => d.KodeTransaksiNavigation).WithMany()
                .HasForeignKey(d => d.KodeTransaksi)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Pesanan_Transaksi");

            entity.HasOne(d => d.NikNavigation).WithMany()
                .HasForeignKey(d => d.Nik)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Pesanan_MasterEmployee");
        });

        modelBuilder.Entity<Transaksi>(entity =>
        {
            entity.HasKey(e => e.KodeTransaksi);

            entity.ToTable("Transaksi");

            entity.Property(e => e.KodeTransaksi)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Id).ValueGeneratedOnAdd();
            entity.Property(e => e.Nik).HasColumnName("NIK");
            entity.Property(e => e.TanggalPembayaran).HasColumnType("datetime");
            entity.Property(e => e.TotalPembayaran).HasColumnType("decimal(18, 0)");

            entity.HasOne(d => d.NikNavigation).WithMany(p => p.Transaksis)
                .HasForeignKey(d => d.Nik)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Transaksi_MasterEmployee");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
