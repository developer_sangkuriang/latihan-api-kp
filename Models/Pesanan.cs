﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace WebAPI_NETCore.Models;


[Table("Pesanan")]
public partial class Pesanan
{
    [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
    public long Id { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string KodeTransaksi { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string KodePemesan { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string KodeMenu { get; set; }

    public long JumlahPesanan { get; set; }

    public decimal HargaSatuan { get; set; }

    public long Nik { get; set; }

    public virtual MasterMenu KodeMenuNavigation { get; set; } = null!;

    public virtual DaftarPemesan KodePemesanNavigation { get; set; } = null!;

    public virtual Transaksi KodeTransaksiNavigation { get; set; } = null!;

    public virtual MasterEmployee NikNavigation { get; set; } = null!;
}

//public partial class Pesanan
//{
//    public long Id { get; set; }
//    public string KodeTransaksi { get; set; } = null!;
//    public string KodePemesan { get; set; } = null!;
//    public string KodeMenu { get; set; } = null!;
//    public long JumlahPesanan { get; set; }
//    public decimal HargaSatuan { get; set; }
//    public long Nik { get; set; }
//    public virtual MasterMenu KodeMenuNavigation { get; set; } = null!;
//    public virtual DaftarPemesan KodePemesanNavigation { get; set; } = null!;
//    public virtual Transaksi KodeTransaksiNavigation { get; set; } = null!;
//    public virtual MasterEmployee NikNavigation { get; set; } = null!;
//}
