﻿using System;
using System.Collections.Generic;

namespace WebAPI_NETCore.Models;

public partial class Transaksi
{
    public long Id { get; set; }

    public string KodeTransaksi { get; set; } = null!;

    public decimal TotalPembayaran { get; set; }

    public bool StatusPembayaran { get; set; }

    public DateTime TanggalPembayaran { get; set; }

    public long Nik { get; set; }

    public virtual MasterEmployee NikNavigation { get; set; } = null!;
}
