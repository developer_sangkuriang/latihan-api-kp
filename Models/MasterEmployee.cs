﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebAPI_NETCore.Models;

[Table("MasterEmployee")]
public partial class MasterEmployee
{
    [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
    //static long AutoId = 0;
    public long? Id { get; set; } //= ++AutoId;

    //[Key]
    public long? Nik { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? NamaLengkap { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? NamaPanggilan { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? TempatLahir { get; set; }

    public DateTime? TanggalLahir { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? JenisKelamin { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? AlamatLengkap { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? AlamatDomisili { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? NomerTelepon { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? Email1 { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? Email2 { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? Jabatan { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string UsernameLogin { get; set; } = null!;

    [StringLength(50)]
    [Unicode(false)]
    public string PasswordLogin { get; set; } = null!;

    [StringLength(50)]
    [Unicode(false)]
    public string HakAkses { get; set; } = null!;

    public bool? IsActive { get; set; }
    public virtual ICollection<HistorySignIn> HistorySignIns { get; set; } = new List<HistorySignIn>();
    public virtual ICollection<Transaksi> Transaksis { get; set; } = new List<Transaksi>();
}

//public partial class MasterEmployee
//{
//    public long Id { get; set; }
//    public long Nik { get; set; }
//    public string? NamaLengkap { get; set; }
//    public string? NamaPanggilan { get; set; }
//    public string? TempatLahir { get; set; }
//    public DateTime? TanggalLahir { get; set; }
//    public string? JenisKelamin { get; set; }
//    public string? AlamatLengkap { get; set; }
//    public string? AlamatDomisili { get; set; }
//    public string? NomerTelepon { get; set; }
//    public string? Email1 { get; set; }
//    public string? Email2 { get; set; }
//    public string? Jabatan { get; set; }
//    public string UsernameLogin { get; set; } = null!;
//    public string PasswordLogin { get; set; } = null!;
//    public string HakAkses { get; set; } = null!;
//    public bool? IsActive { get; set; }
//    public virtual ICollection<HistorySignIn> HistorySignIns { get; set; } = new List<HistorySignIn>();
//    public virtual ICollection<Transaksi> Transaksis { get; set; } = new List<Transaksi>();
//}