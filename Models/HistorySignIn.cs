﻿using System;
using System.Collections.Generic;

namespace WebAPI_NETCore.Models;

public partial class HistorySignIn
{
    public long Id { get; set; }

    public long Nik { get; set; }

    public DateTime LastLogged { get; set; }

    public virtual MasterEmployee NikNavigation { get; set; } = null!;
}
