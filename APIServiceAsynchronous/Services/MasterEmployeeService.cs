﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI_NETCore.Connection;
using WebAPI_NETCore.Controllers;
using WebAPI_NETCore.Models;

namespace APIServiceAsynchronous.Services
{
    public partial class MasterEmployeeService
    {
        public static ApplicationDBContext DBContext = new ApplicationDBContext();

		public async Task<IQueryable> GetAllData()
		{
			try
			{
				var Data = DBContext.MasterMenus.OrderBy(x => x.Id).AsNoTracking().AsQueryable();
				return Data;
			}
			catch (NullReferenceException ErrorNullReference)
			{
				Console.WriteLine(ErrorNullReference.Message);
				throw;
			}
			catch (Exception ErrorException)
			{
				Console.WriteLine(ErrorException.Message);
				throw new ApplicationException(ErrorException.Message);
			}
			finally
			{
			}
		}
	}
}
