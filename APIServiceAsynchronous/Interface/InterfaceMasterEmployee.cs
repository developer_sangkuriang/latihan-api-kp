﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI_NETCore.Connection;
using WebAPI_NETCore.Controllers;
using WebAPI_NETCore.Models;

namespace APIServiceAsynchronous.Interface
{
    public interface InterfaceMasterEmployee
    {
        Task<IQueryable> GetAllData();
    }
}
