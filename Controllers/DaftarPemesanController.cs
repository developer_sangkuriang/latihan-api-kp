﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI_NETCore.Connection;
using WebAPI_NETCore.Models;

namespace WebAPI_NETCore.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DaftarPemesanController : ControllerBase
    {
        #region Property Connection Database
        private readonly ApplicationDBContext DBContext;
        private readonly ILogger<DaftarPemesanController> Logger;

        public DaftarPemesanController(ILogger<DaftarPemesanController> logger, ApplicationDBContext dbcontext)
        {
            Logger = logger;
            DBContext = dbcontext;
        }
        #endregion

        [HttpDelete("{KodePemesan}"), Produces("application/json")]
        public IActionResult DeleteDataByKodePemesan(string KodePemesan)
        {
            #region Example : Delete One Data Or Delete Single Data
            var GetData = DBContext.DaftarPemesans.Where(x => x.KodePemesan == KodePemesan).FirstOrDefault();
            DBContext.DaftarPemesans.Entry(GetData).State = EntityState.Deleted;
            DBContext.SaveChanges();
            object Result = new
            {
                GetData
            };
            #endregion
            
            return Ok(GetData);

        }

        [HttpDelete, Produces("application/json")]
        public IActionResult DeleteDataMultipleByKodePemesan(List<string> KodePemesan)
        {
            #region Example : Delete More Than One (Delete Multiple Data)
            var TemporaryData = new DaftarPemesan();
            var ListDataDelete = new List<DaftarPemesan>();
            var StringJoin = string.Join(",", KodePemesan.ToList());
            var SplitData = StringJoin.Split(",");
            foreach (var obj in SplitData)
            {
                var GetData = (from x in DBContext.DaftarPemesans.Where(x => x.KodePemesan.Contains(obj))
                               select new DaftarPemesan
                               {
                                   Id = x.Id,
                                   KodePemesan = x.KodePemesan,
                                   NamaPemesan = x.NamaPemesan,
                                   KodeMenu = x.KodeMenu
                               }).AsQueryable();

                foreach (var x in GetData)
                {
                    TemporaryData.Id = x.Id;
                    TemporaryData.KodePemesan = x.KodePemesan;
                    TemporaryData.NamaPemesan = x.NamaPemesan;
                    TemporaryData.KodeMenu = x.KodeMenu;
                    ListDataDelete.Add(TemporaryData);

                    DBContext.DaftarPemesans.Entry(TemporaryData).State = EntityState.Deleted;
                    DBContext.SaveChanges();
                }
            }

            object Result = new
            {
                ListDataDelete
            };
            #endregion

            return Ok(Result);
        }

        [HttpGet("{KodeMenu}"), Produces("application/json")]
        public IActionResult GetDataByKodePemesan(string kodePemesan)
        {
            #region Example : Get One Data Or Get Single Data
            var GetData = DBContext.DaftarPemesans.FirstOrDefault(x => x.KodePemesan == kodePemesan);
            object Result = new
            {
                GetData
            };
            #endregion

            return Ok(GetData);
        }
        [HttpGet, Produces("application/json")]
        public IActionResult GetDataMultipleString(List<string> Payload)
        {
            var ListData = new List<dynamic>();
            var StringJoin = string.Join(",", Payload);
            var SplitData = StringJoin.Split(",");
            foreach (var Obj in SplitData)
            {
                var GetData = DBContext.DaftarPemesans.Where(x => x.KodePemesan == Obj).SingleOrDefault();
                ListData.Add(GetData);
            }
            return Ok(ListData);
        }

        [HttpGet, Produces("application/json")]
        public IActionResult GetDataMultipleInt(List<int> Payload)
        {
            var ListData = new List<dynamic>();
            var StringJoin = string.Join(",", Payload);
            var SplitData = StringJoin.Split(",");
            foreach (var Obj in SplitData)
            {
                var GetData = DBContext.DaftarPemesans.Where(x => x.Id == Convert.ToInt16(Obj)).SingleOrDefault();
                ListData.Add(GetData);
            }
            return Ok(ListData);
        }

        /// <summary>
        /// MasterEmployee API CRUD : Create, Read, Update, Delete --------------------------------------------------------------------------------------------//
        /// </summary>



        /// Create your API code here

    }
}
