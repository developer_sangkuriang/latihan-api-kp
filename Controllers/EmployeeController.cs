﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using WebAPI_NETCore.Models;
using static Azure.Core.HttpHeader;


namespace WebAPI_NETCore.Controllers
{
    [Route("API/[controller]/[action]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        #region Data Employee Yang Belum Dibungkus Dengan Action / Method

        IList<Employee> employees = new List<Employee>()
        {
            new Employee()
            {
                EmployeeId = 1, EmployeeName = "Mochammad Reza Fahlevi", Address = "Cilegon", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 2, EmployeeName = "Mario Kevin Binuan", Address = "Bengkulu", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 3, EmployeeName = "Ardelingga Ngopi Masseeeh", Address = "Cirebon", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 4, EmployeeName = "Fadil Kudil", Address = "Tangerang", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 5, EmployeeName = "Badrul Salim", Address = "Pandeglang", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 6, EmployeeName = "Abdul Kirom Wong Tegal", Address = "Tegal", Department = "Support"
            },
            new Employee()
            {
                EmployeeId = 7, EmployeeName = "Arren Rediman", Address = "Medan", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 8, EmployeeName = "Virda Jeeehh", Address = "Indramayu", Department = "KP"
            },
            new Employee()
            {
                EmployeeId = 9, EmployeeName = "Ayu Ayunan", Address = "Indramayu", Department = "KP"
            },
            new Employee()
            {
                EmployeeId = 10, EmployeeName = "Dinda Denda", Address = "Indramayu", Department = "KP"
            },
            new Employee()
            {
                EmployeeId = 11, EmployeeName = "Ninis Mangga Harum Manis", Address = "Indramayu", Department = "KP"
            },
        };

        #endregion

        #region Data Employee Yang Sudah Dibungkus Dengan Action / Method

        [HttpGet, Produces("application/json")]
        public async Task<List<Employee>> DataList()
        {
            var Data = new List<Employee>()
            {
                new Employee()
                {
                    EmployeeId = 1, EmployeeName = "Mochammad Reza Fahlevi", Address = "Cilegon", Department = "IT"
                },
                new Employee()
                {
                    EmployeeId = 2, EmployeeName = "Mario Kevin Binuan", Address = "Bengkulu", Department = "IT"
                },
                new Employee()
                {
                    EmployeeId = 3, EmployeeName = "Ardelingga Ngopi Masseeeh", Address = "Cirebon", Department = "IT"
                },
                new Employee()
                {
                    EmployeeId = 4, EmployeeName = "Fadil Kudil", Address = "Tangerang", Department = "IT"
                },
                new Employee()
                {
                    EmployeeId = 5, EmployeeName = "Badrul Salim", Address = "Pandeglang", Department = "IT"
                },
                new Employee()
                {
                    EmployeeId = 6, EmployeeName = "Abdul Kirom Wong Tegal", Address = "Tegal", Department = "Support"
                },
                new Employee()
                {
                    EmployeeId = 7, EmployeeName = "Arren Rediman", Address = "Medan", Department = "IT"
                },
                new Employee()
                {
                    EmployeeId = 8, EmployeeName = "Virda Jeeehh", Address = "Indramayu", Department = "KP"
                },
                new Employee()
                {
                    EmployeeId = 9, EmployeeName = "Ayu Ayunan", Address = "Indramayu", Department = "KP"
                },
                new Employee()
                {
                    EmployeeId = 10, EmployeeName = "Dinda Denda", Address = "Indramayu", Department = "KP"
                },
                new Employee()
                {
                    EmployeeId = 11, EmployeeName = "Ninis Mangga Harum Manis", Address = "Indramayu", Department = "KP"
                },
            };
            return Data;
            //return Data.ToList();
        }

        [HttpGet, Produces("application/json")]
        public async Task<IQueryable> DataIQueryable()
        {
            var Data = new List<Employee>()
            {
                new Employee()
                {
                    EmployeeId = 1, EmployeeName = "Mochammad Reza Fahlevi", Address = "Cilegon", Department = "IT"
                },
                new Employee()
                {
                    EmployeeId = 2, EmployeeName = "Mario Kevin Binuan", Address = "Bengkulu", Department = "IT"
                },
                new Employee()
                {
                    EmployeeId = 3, EmployeeName = "Ardelingga Ngopi Masseeeh", Address = "Cirebon", Department = "IT"
                },
                new Employee()
                {
                    EmployeeId = 4, EmployeeName = "Fadil Kudil", Address = "Tangerang", Department = "IT"
                },
                new Employee()
                {
                    EmployeeId = 5, EmployeeName = "Badrul Salim", Address = "Pandeglang", Department = "IT"
                },
                new Employee()
                {
                    EmployeeId = 6, EmployeeName = "Abdul Kirom Wong Tegal", Address = "Tegal", Department = "Support"
                },
                new Employee()
                {
                    EmployeeId = 7, EmployeeName = "Arren Rediman", Address = "Medan", Department = "IT"
                },
                new Employee()
                {
                    EmployeeId = 8, EmployeeName = "Virda Jeeehh", Address = "Indramayu", Department = "KP"
                },
                new Employee()
                {
                    EmployeeId = 9, EmployeeName = "Ayu Ayunan", Address = "Indramayu", Department = "KP"
                },
                new Employee()
                {
                    EmployeeId = 10, EmployeeName = "Dinda Denda", Address = "Indramayu", Department = "KP"
                },
                new Employee()
                {
                    EmployeeId = 11, EmployeeName = "Ninis Mangga Harum Manis", Address = "Indramayu", Department = "KP"
                },
            };
            return Data.AsQueryable();
        }

        #endregion

        #region Example API Latihan

        [HttpGet("{Name}"), Produces("application/json")]
        public IActionResult GetDataByName(string Name)
        {
            #region Example List : Get Data Using LINQ 
            var Data_LINQ = (from x in employees where x.EmployeeName.Contains(Name) select x).ToList();
            #endregion

            #region Example List : Get Data Using Entity Framework
            var Data_EF = employees.Where(x => x.EmployeeName.Contains(Name)).ToList();
            #endregion

            object Result = new
            {
                Data_LINQ,
                Data_EF
            };

            return Ok(Result);
        }

        [HttpGet("{Id}/{Name}/{Address}"), Produces("application/json")]
        public IActionResult GetDataByMultipleParameter(int Id, string Name, string Address)
        {
            #region Example List : Get Data Using LINQ 
            var Data_LINQ = (from x in employees where x.EmployeeId == Id && x.EmployeeName.Contains(Name) select x).ToList();
            #endregion

            #region Example List : Get Data Using Entity Framework
            var Data_EF = employees.Where(x => x.EmployeeId == Id && x.EmployeeName.Contains(Name)).ToList();
            #endregion

            object Result = new
            {
                Data_LINQ,
                Data_EF
            };

            return Ok(Result);
        }

        [HttpGet, Produces("application/json")]
        public IActionResult GetDataByListString(List<string> ListData)
        {
            List<dynamic> Temporary = new List<dynamic>();
            var StringJoin = String.Join(",", ListData.ToList());
            var SplitData = StringJoin.Split(",");
            foreach (var obj in SplitData)
            {
                dynamic GetData = (from x in employees select x).Where(x => x.EmployeeName.Contains(obj)).ToList();
                Temporary.Add(GetData);
            }
            return Ok(Temporary);
        }

        [HttpGet, Produces("application/json")]
        public IActionResult GetDataByListInteger(List<int> ListData)
        {
            List<dynamic> Temporary = new List<dynamic>();
            foreach (int obj in ListData)
            {
                dynamic GetData = (from x in employees select x).Where(x => x.EmployeeId == obj).ToList();
                Temporary.Add(GetData);
            }
            return Ok(Temporary);
        }

        #endregion




        /// <summary>
        /// Example API CRUD : Create, Read, Update, Delete --------------------------------------------------------------------------------------------//
        /// </summary>

        #region Example API CRUD : Create, Read, Update, Delete

        [HttpPost, Produces("application/json")]
        public IActionResult AddData(Employee Payload)
        {
            var DataBefore = employees.Count();
            employees.Add(Payload);
            var DataAfter = employees.Count();
            return Ok(employees);
        }

        [HttpGet, Produces("application/json")]
        public IActionResult GetAllDataEmployee()
        {
            var Result = DataList();
            return Ok(Result.Result);
        }

        [HttpGet("{Id:int}"), Produces("application/json")]
        public IActionResult GetDataById(int Id)
        {
            var Result_FirstOrDefault = employees.FirstOrDefault(x => x.EmployeeId == Id);
            var Result_WithSingle = employees.Single(x => x.EmployeeId == Id);
            var Result_WithFirst = employees.First(x => x.EmployeeId == Id);
            var Result_WithWhere = employees.Where(x => x.EmployeeId == Id);

            object Result = new
            {
                Result_FirstOrDefault,
                Result_WithSingle,
                Result_WithFirst,
                Result_WithWhere
            };

            return Ok(Result);
        }

        [HttpPost, Produces("application/json")]
        public IActionResult UpdateData(Employee Payload)
        {
            #region Example 1 : Update data untuk dan dari bentuk list
            var List_Data = employees.Where(x => x.EmployeeId == Payload.EmployeeId).ToList();

            foreach (var x in List_Data)
            {
                x.EmployeeId = Payload.EmployeeId;
                x.EmployeeName = Payload.EmployeeName;
                x.Address = Payload.Address;
                x.Department = Payload.Department;
            }
            return Ok(List_Data);
            #endregion

            #region Example 2 : Update data untuk dan dari bentuk single
            //var Single_Data = employees.Where(x => x.EmployeeId == Payload.EmployeeId).FirstOrDefault();

            //Single_Data.EmployeeId = Payload.EmployeeId;
            //Single_Data.EmployeeName = Payload.EmployeeName;
            //Single_Data.Address = Payload.Address;
            //Single_Data.Department = Payload.Department;
            //return Ok(Single_Data);
            #endregion
        }

        [HttpDelete("{Id:int}"), Produces("application/json")]
        public IActionResult DeleteDataById(int Id)
        {
            #region Example List : Get Data Using LINQ 
            var Data = (from x in employees select x).ToList();
            #endregion

            var Result = Data.RemoveAll(x => x.EmployeeId == Id);

            return Ok(Data);
        }

        #endregion

    }
}
