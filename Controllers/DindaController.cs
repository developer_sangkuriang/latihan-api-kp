﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI_NETCore.Models;

namespace WebAPI_NETCore.Controllers
{
    [Route("API/[controller]/[action]")]
    [ApiController]
    public class DindaController : ControllerBase
    {
        #region Data

        IList<Employee> employees = new List<Employee>()
        {
            new Employee()
            {
                EmployeeId = 1, EmployeeName = "Mochammad Reza Fahlevi", Address = "Cilegon", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 2, EmployeeName = "Mario Kevin Binuan", Address = "Bengkulu", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 3, EmployeeName = "Ardelingga Ngopi Masseeeh", Address = "Cirebon", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 4, EmployeeName = "Fadil Kudil", Address = "Tangerang", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 5, EmployeeName = "Badrul Salim", Address = "Pandeglang", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 6, EmployeeName = "Abdul Kirom Wong Tegal", Address = "Tegal", Department = "Support"
            },
            new Employee()
            {
                EmployeeId = 7, EmployeeName = "Arren Rediman", Address = "Medan", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 8, EmployeeName = "Virda Jeeehh", Address = "Indramayu", Department = "KP"
            },
            new Employee()
            {
                EmployeeId = 9, EmployeeName = "Ayu Ayunan", Address = "Indramayu", Department = "KP"
            },
            new Employee()
            {
                EmployeeId = 10, EmployeeName = "Dinda Denda", Address = "Indramayu", Department = "KP"
            },
            new Employee()
            {
                EmployeeId = 11, EmployeeName = "Ninis Mangga Harum Manis", Address = "Indramayu", Department = "KP"
            },
        };

        #endregion


        #region API Dinda

        [HttpGet, Produces("application/json")]
        public IActionResult Quiz()
        {
            /* Quiz : 
            Ambil data yang memiliki Department "KP", 
            lalu jumlahkan berapa banyak orang yang berada di'department KP..
            kemudian tampilkan hasil response tersebut pada postman ?

            * Library .NET : "Where" dan "Sum"
            * References : 
            - https://linqexamples.com/projections/select.html#query-syntax-2
            - https://www.tutorialsteacher.com/linq/linq-query-syntax
            - https://dotnettutorials.net/lesson/where-filtering-operators-in-linq/?expand_article=1
            - https://www.geeksforgeeks.org/linq-filtering-operator-where/
            - https://www.csharp-examples.net/linq-sum/
            - https://dotnettutorials.net/lesson/linq-sum-method/
            - https://www.csharptutorial.net/csharp-linq/linq-sum/
            */

            #region Please enter the coding answer below
            var Department_KP = (from x in employees
                                 where x.Department.Contains("KP")
                                 select new
                                 {
                                     EmployeeId = x.EmployeeId,
                                     EmployeeName = x.EmployeeName,
                                     Address = x.Address,
                                     Department = x.Department
                                 }).ToList();

            int Total_Person_In_Department_KP = Department_KP.Count();

            var Total_Sum_From_Object_EmployeeId = Department_KP.Sum(x => x.EmployeeId);

            object Result = new
            {
                Department_KP,
                Total_Person_In_Department_KP,
                Total_Sum_From_Object_EmployeeId
            };


            return Ok(Result);
            #endregion


        }

        #endregion
    }
}
