﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.Entity;
using System.Drawing;
using System.Linq.Dynamic.Core;
using WebAPI_NETCore.Connection;
using WebAPI_NETCore.Models;
using static System.Runtime.InteropServices.JavaScript.JSType;
using EntityState = Microsoft.EntityFrameworkCore.EntityState;

namespace WebAPI_NETCore.Controllers
{
    [Route("API/[controller]/[action]")]
    [ApiController]
    public class ExampleConnectDatabaseController : ControllerBase
    {
        #region Property Connection Database
        private readonly ApplicationDBContext DBContext;
        private readonly ILogger<ExampleConnectDatabaseController> Logger;

        public ExampleConnectDatabaseController(ILogger<ExampleConnectDatabaseController> logger, ApplicationDBContext dbcontext)
        {
            Logger = logger;
            DBContext = dbcontext;
        }
        #endregion

        /// <summary>
        /// Example API CRUD : Create, Read, Update, Delete --------------------------------------------------------------------------------------------//
        /// </summary>

        [HttpGet, Produces("application/json")]
        public IActionResult GetAllDataMasterMenu()
        {
            var GetData_WithList = DBContext.MasterMenus.OrderBy(x => x.Id).ToList();

            return Ok(GetData_WithList);
        }

        [HttpGet, Produces("application/json")]
        public IActionResult GetAllDataMasterMenuAsQueryable()
        {
            var GetData_WithAsQueryable = DBContext.MasterMenus.OrderBy(x => x.Id).AsQueryable();
            int Hitung = GetData_WithAsQueryable.Count();

            #region Example Get Data with LINQ and use condition if else on the property model
            //var GetData = DBContext.MasterMenus.Select(Hasil => new MasterMenu
            //{
            //    Id = Hasil.Id,
            //    KodeMenu = Hasil.KodeMenu,
            //    NamaMenu = Hasil.NamaMenu,
            //    JenisMenu = Hasil.JenisMenu == "Food" ? "Food" : "Drink",
            //    HargaSatuan = Hasil.HargaSatuan
            //}).Where(x => x.Id == 16).Single();
            ///*
            //--> 
            //--> Tanda Baca ? adalah If
            //--> Tanda Baca : adalah Else 
            //--> Hasil.JenisMenu == "Food" saja dengan GetData.JenisMenu == JenisFood
            // */
            //var JenisFood = "Food";
            //var JenisDrink = "Drink";
            //var Jenis = string.Empty;
            //if (GetData.JenisMenu == JenisFood || GetData.JenisMenu.Contains(JenisFood))
            //{
            //    Jenis = JenisFood;
            //}
            //else
            //{
            //    Jenis = JenisDrink;
            //}
            #endregion

            return Ok(GetData_WithAsQueryable);
        }

        [HttpPost, Produces("application/json")]
        public IActionResult AddData(MasterMenu Payload)
        {
            #region Membuat atau menyiapkan penampung dalam bentuk single temporary dengan object MasterMenu, and call the database connection for added the data, kemudian menampilkan hasil data yang sudah diupdate kedalam return.
            var TemporaryDataInsert = new MasterMenu()
            {

                KodeMenu = Payload.KodeMenu,
                NamaMenu = Payload.NamaMenu,
                JenisMenu = Payload.JenisMenu,
                HargaSatuan = Payload.HargaSatuan
            };
            #endregion

            #region Call the database connection and then insert data.
            DBContext.Add(TemporaryDataInsert);
            DBContext.SaveChanges();
            #endregion

            return Ok(TemporaryDataInsert);
        }

        [HttpPost, Produces("application/json")]
        public IActionResult AddDataMultiple(List<MasterMenu> Payload)
        {
            #region Membuat atau menyiapkan penampung data lebih dari satu menggunakan list dan perulangan foreach, and call the database connection for added multiple the data, kemudian menampilkan hasil data yang sudah ditambah kedalam return.
            var List_Data = new List<dynamic>();
            var TemporaryDataInsert = new List<MasterMenu>();
            var DBContext = new ApplicationDBContext();
            foreach (var x in Payload)
            {
                TemporaryDataInsert.Add(new MasterMenu
                {
                    KodeMenu = x.KodeMenu,
                    NamaMenu = x.NamaMenu,
                    JenisMenu = x.JenisMenu,
                    HargaSatuan = x.HargaSatuan
                });
            }

            List_Data.AddRange(TemporaryDataInsert);
            #endregion

            #region Call the database connection and then insert multiple data.
            DBContext.MasterMenus.AddRange(TemporaryDataInsert);
            DBContext.SaveChanges();
            #endregion
             
            return Ok(List_Data);
        }

        [HttpPost, Produces("application/json")]
        public IActionResult UpdateData(MasterMenu Payload)
        {
            #region Example 1 With List Data

            #region Mengambil data dari table dalam bentuk AsQueryable / ToList, dimana property where yang digunakan sebagai pencarian yaitu KodeMenu
            //var GetData = DBContext.MasterMenus.Where(x => x.KodeMenu == Payload.KodeMenu).AsQueryable();
            #endregion

            #region Membuat perulangan dalam bentuk foreach yang digunakan untuk melakukan update data, dari data yang sudah di'ditemukan atau sudah diambil dari table, kemudian menampilkan hasil data yang sudah diupdate kedalam return.
            //foreach (var x in GetData)
            //{
            //    if (GetData != null || GetData.Count() > 0) // <-- Checking if the data already exists
            //    {
            //        x.KodeMenu = Payload.KodeMenu;
            //        x.NamaMenu = Payload.NamaMenu;
            //        x.JenisMenu = Payload.JenisMenu;
            //        x.HargaSatuan = Payload.HargaSatuan;
            //        DBContext.SaveChanges();
            //    }
            //}
            //return Ok(Payload);
            #endregion

            #endregion

            #region Example 2 With Single Data 

            #region Mengambil data hanya satu menggunakan FirstOrDefault, dimana pada FirstOrDefault property yang digunakan sebagai pencarian yaitu KodeMenu
            var GetData = DBContext.MasterMenus.FirstOrDefault(x => x.KodeMenu == Payload.KodeMenu);
            //var GetData = DBContext.MasterMenus.Where(x => x.KodeMenu == Payload.KodeMenu).FirstOrDefault();
            #endregion

            #region Checking if the data already exists and call the database connection for modify the data, kemudian menampilkan hasil data yang sudah diupdate kedalam return.
            if (GetData != null || GetData.KodeMenu != null || GetData.KodeMenu == Payload.KodeMenu || GetData.KodeMenu.Contains(Payload.KodeMenu))
            {
                GetData.KodeMenu = Payload.KodeMenu;
                GetData.NamaMenu = Payload.NamaMenu;
                GetData.JenisMenu = Payload.JenisMenu;
                GetData.HargaSatuan = Payload.HargaSatuan;
                DBContext.SaveChanges();
            }
            #endregion

            return Ok(Payload);

            #endregion
        }

        [HttpPatch, Produces("application/json")]
        public IActionResult UpdateDataMultiple(List<MasterMenu> Payload)
        {
            #region Membuat atau menyiapkan penampung dalam bentuk list dynamic
            var ListData_After = new List<MasterMenu>();
            #endregion

            #region Mengambil data dari table dengan kondisi where dengan payload yang dikirim, untuk property where yang digunakan sebagai pencarian yaitu KodeMenu. Kemudian data yang sudah diambil akan dan akan disimpan kedalam penampung berbentuk List.
            var GetData_Before = DBContext.MasterMenus.Where(p => Payload.Select(x => x.KodeMenu).Contains(p.KodeMenu)).Select(Hasil => new MasterMenu
            {
                Id = Hasil.Id,
                KodeMenu = Hasil.KodeMenu,
                NamaMenu = Hasil.NamaMenu,
                JenisMenu = Hasil.JenisMenu,
                HargaSatuan = Hasil.HargaSatuan,
            }).ToList();
            #endregion

            #region Membuat perulangan dalam bentuk foreach yang digunakan untuk melakukan update data, dari data yang sudah di'ditemukan atau sudah diambil dari table.
            foreach (var Obj in GetData_Before)
            {
                #region Object A : Membuat object dalam bentuk variable untuk : Id, KodeMenu, NamaMenu, JenisMenu, HargaSatuan.
                var Id = GetData_Before.Where(x => x.KodeMenu == Obj.KodeMenu).FirstOrDefault().Id;
                var KodeMenu = Payload?.Where(x => x?.KodeMenu == Obj?.KodeMenu).FirstOrDefault()?.KodeMenu;
                var NamaMenu = Payload?.Where(x => x?.KodeMenu == Obj?.KodeMenu).FirstOrDefault()?.NamaMenu;
                var JenisMenu = Payload?.Where(x => x?.KodeMenu == Obj?.KodeMenu).FirstOrDefault()?.JenisMenu;
                var HargaSatuan = Payload?.Where(x => x.KodeMenu == Obj.KodeMenu).FirstOrDefault()?.HargaSatuan;
                #endregion

                #region Object B : Membuat object tampung dari object A dalam bentuk variable dan akan disimpan kedalam penampung berbentuk List untuk object B.
                var TemporaryDataAfter = new MasterMenu
                {
                    Id = Id,
                    KodeMenu = KodeMenu,
                    NamaMenu = NamaMenu,
                    JenisMenu = JenisMenu,
                    HargaSatuan = HargaSatuan,
                };
                ListData_After.Add(TemporaryDataAfter);
                #endregion

                #region Call the database connection and then modify the data.
                DBContext.Update(TemporaryDataAfter).Property(x => x.Id).IsModified = false;
                DBContext.SaveChanges();
                #endregion
            }
            #endregion

            #region Membuat object dalam bentuk variable dynamic, untuk menampilkan hasil data yang sudah diupdate kedalam return.
            dynamic Result = new
            {
                GetData_Before,
                ListData_After
            };
            #endregion

            return Ok(Result);
        }

        [HttpDelete("{KodeMenu}"), Produces("application/json")]
        public IActionResult DeleteDataByKodeMenu(string KodeMenu)
        {
            #region Example : Delete One Data Or Delete Single Data
            var GetData = DBContext.MasterMenus.Where(x => x.KodeMenu == KodeMenu).FirstOrDefault();
            DBContext.MasterMenus.Entry(GetData).State = EntityState.Deleted;
            DBContext.SaveChanges();
            object Result = new
            {
                GetData
            };
            #endregion

            return Ok(GetData);

        }

        [HttpDelete, Produces("application/json")]
        public IActionResult DeleteDataMultipleByKodeMenu(List<string> KodeMenu)
        {
            #region Example : Delete More Than One (Delete Multiple Data)
            var TemporaryData = new MasterMenu();
            var ListDataDelete = new List<MasterMenu>();
            var StringJoin = string.Join(",", KodeMenu.ToList());
            var SplitData = StringJoin.Split(",");
            foreach (var obj in SplitData)
            {
                var GetData = (from x in DBContext.MasterMenus.Where(x => x.KodeMenu.Contains(obj)) select new MasterMenu
                {
                    Id = x.Id,
                    KodeMenu = x.KodeMenu,
                    NamaMenu = x.NamaMenu,
                    JenisMenu = x.JenisMenu,
                    HargaSatuan = x.HargaSatuan
                }).AsQueryable();

                foreach (var x in GetData)
                {
                    TemporaryData.Id = x.Id;
                    TemporaryData.KodeMenu = x.KodeMenu;
                    TemporaryData.NamaMenu = x.NamaMenu;
                    TemporaryData.JenisMenu = x.JenisMenu;
                    TemporaryData.HargaSatuan = x.HargaSatuan;
                    ListDataDelete.Add(TemporaryData);

                    DBContext.MasterMenus.Entry(TemporaryData).State = EntityState.Deleted;
                    DBContext.SaveChanges();
                }
            }

            object Result = new
            {
                ListDataDelete
            };
            #endregion

            return Ok(Result);
        }

        [HttpGet("{KodeMenu}"), Produces("application/json")]
        public IActionResult GetDataByKodeMenu(string kodeMenu)
        {
            #region Example : Get One Data Or Get Single Data
            var GetData = DBContext.MasterMenus.FirstOrDefault(x => x.KodeMenu == kodeMenu);
            object Result = new
            {
                GetData
            };
            #endregion

            return Ok(GetData);
        }

        [HttpGet, Produces("application/json")]
        public IActionResult GetDataMultipleString(List<string> Payload)
        {
            var ListData = new List<dynamic>();
            var StringJoin = string.Join(",", Payload);
            var SplitData = StringJoin.Split(",");
            foreach (var Obj in SplitData)
            {
                var GetData = DBContext.MasterMenus.Where(x => x.KodeMenu == Obj).SingleOrDefault();
                ListData.Add(GetData);
            }
            return Ok(ListData);
        }

        [HttpGet, Produces("application/json")]
        public IActionResult GetDataMultipleInt(List<int> Payload)
        {
            var ListData = new List<dynamic>();
            var StringJoin = string.Join(",", Payload);
            var SplitData = StringJoin.Split(",");
            foreach (var Obj in SplitData)
            {
                var GetData = DBContext.MasterMenus.Where(x => x.Id == Convert.ToInt16(Obj)).SingleOrDefault();
                ListData.Add(GetData);
            }
            return Ok(ListData);
        }
    }


}