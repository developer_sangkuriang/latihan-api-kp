﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using WebAPI_NETCore.Models;

namespace WebAPI_NETCore.Controllers
{
    [Route("API/[controller]/[action]")]
    [ApiController]
    public class AyuController : ControllerBase
    {
        #region Data

        IList<Employee> employees = new List<Employee>()
        {
            new Employee()
            {
                EmployeeId = 1, EmployeeName = "Mochammad Reza Fahlevi", Address = "Cilegon", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 2, EmployeeName = "Mario Kevin Binuan", Address = "Bengkulu", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 3, EmployeeName = "Ardelingga Ngopi Masseeeh", Address = "Cirebon", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 4, EmployeeName = "Fadil Kudil", Address = "Tangerang", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 5, EmployeeName = "Badrul Salim", Address = "Pandeglang", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 6, EmployeeName = "Abdul Kirom Wong Tegal", Address = "Tegal", Department = "Support"
            },
            new Employee()
            {
                EmployeeId = 7, EmployeeName = "Arren Rediman", Address = "Medan", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 8, EmployeeName = "Virda Jeeehh", Address = "Indramayu", Department = "KP"
            },
            new Employee()
            {
                EmployeeId = 9, EmployeeName = "Ayu Ayunan", Address = "Indramayu", Department = "KP"
            },
            new Employee()
            {
                EmployeeId = 10, EmployeeName = "Dinda Denda", Address = "Indramayu", Department = "KP"
            },
            new Employee()
            {
                EmployeeId = 11, EmployeeName = "Ninis Mangga Harum Manis", Address = "Indramayu", Department = "KP"
            },
        };

        #endregion


        #region API Ayu

        [HttpGet, Produces("application/json")]
        public IActionResult Quiz()
        {
            /* Quiz
            Cari data yang memiliki Address "Indramayu", 
            lalu grouping semua data berdasarkan Address..
            kemudian tampilkan EmployeeName dan Address, sehingga akan menampilkan hasil "4 nama dari EmployeeName dan 1 nama dari Address"

            * Ouput yang dihasilkan seperti dibawah ini :
            - EmployeeName : Virda Jeeehh
            - EmployeeName : Ayu Ayunan
            - EmployeeName : Dinda Denda
            - EmployeeName : Ninis Mangga Harum Manis
            - Address      : Indramayu

            kemudian tampilkan hasil response tersebut pada postman ?

            * Library .NET : "Where" dan "GroupBy"
            * References : 
            - https://linqexamples.com/projections/select.html#query-syntax-2
            - https://www.tutorialsteacher.com/linq/linq-query-syntax
            - https://dotnettutorials.net/lesson/where-filtering-operators-in-linq/?expand_article=1
            - https://www.geeksforgeeks.org/linq-filtering-operator-where/
            - https://stackoverflow.com/questions/7325278/group-by-in-linq
            - https://www.csharptutorial.net/csharp-linq/linq-groupby/
            - https://www.tutorialsteacher.com/linq/linq-grouping-operator-groupby-tolookup
            - https://www.javatpoint.com/linq-groupby-method
            */

            #region Please enter the coding answer below

            var EmployeeName = new List<dynamic>();
            dynamic Department = null;

            //var Data = (from x in employees
            //            where x.Address.Contains("Indramayu")
            //            select new Employee
            //            {
            //                EmployeeId = x.EmployeeId,
            //                EmployeeName = x.EmployeeName,
            //                Address = x.Address,
            //                Department = x.Department
            //            }).ToList();

            var Data_EF = employees.Where(x => x.Department.Contains("KP")).ToList();
            //var Data_EF = employees.Where(x => x.Address.Contains(Address)).ToList();

            EmployeeName.Add(Data_EF);
            Department = Data_EF.GroupBy(x => x.Department).First().Key;

            dynamic Result = new
            {
                EmployeeName,
                Department
            };

            #endregion

            return Ok(Result);
        }

        #endregion

    }
}
