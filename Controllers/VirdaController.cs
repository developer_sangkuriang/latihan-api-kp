﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI_NETCore.Models;

namespace WebAPI_NETCore.Controllers
{
    [Route("API/[controller]/[action]")]
    [ApiController]
    public class VirdaController : ControllerBase
    {
        #region Data

        IList<Employee> employees = new List<Employee>()
        {
            new Employee()
            {
                EmployeeId = 1, EmployeeName = "Mochammad Reza Fahlevi", Address = "Cilegon", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 2, EmployeeName = "Mario Kevin Binuan", Address = "Bengkulu", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 3, EmployeeName = "Ardelingga Ngopi Masseeeh", Address = "Cirebon", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 4, EmployeeName = "Fadil Kudil", Address = "Tangerang", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 5, EmployeeName = "Badrul Salim", Address = "Pandeglang", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 6, EmployeeName = "Abdul Kirom Wong Tegal", Address = "Tegal", Department = "Support"
            },
            new Employee()
            {
                EmployeeId = 7, EmployeeName = "Arren Rediman", Address = "Medan", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 8, EmployeeName = "Virda Jeeehh", Address = "Indramayu", Department = "KP"
            },
            new Employee()
            {
                EmployeeId = 9, EmployeeName = "Ayu Ayunan", Address = "Indramayu", Department = "KP"
            },
            new Employee()
            {
                EmployeeId = 10, EmployeeName = "Dinda Denda", Address = "Indramayu", Department = "KP"
            },
            new Employee()
            {
                EmployeeId = 11, EmployeeName = "Ninis Mangga Harum Manis", Address = "Indramayu", Department = "KP"
            },
        };

        #endregion


        #region API Virda

        [HttpGet, Produces("application/json")]
        public IActionResult Quiz()
        {
            /* Quiz : 
            Ambil data yang memiliki Department "KP dan IT", 
            lalu ambil data 2 dari masing-masing Department..
            kemudian tampilkan hasil response tersebut pada postman ?

            * Library .NET : "Where" dan "Take"
            * References : 
            - https://linqexamples.com/projections/select.html#query-syntax-2
            - https://www.tutorialsteacher.com/linq/linq-query-syntax
            - https://dotnettutorials.net/lesson/where-filtering-operators-in-linq/?expand_article=1
            - https://www.geeksforgeeks.org/linq-filtering-operator-where/
            - https://linuxhint.com/csharp-linq-take/
            - https://linqexamples.com/partitioning/take.html
            - https://stackoverflow.com/questions/15385905/linq-with-skip-and-take
            - https://www.csharptutorial.net/csharp-linq/linq-take/
            */

            #region Please enter the coding answer below
            var DataKP = employees.Where(x => x.Department.Contains("KP")).ToList();
            var DataIT = employees.Where(x => x.Department.Contains("IT")).ToList();
            #endregion

            object Hasil = new
            {
                DataKP,
                DataIT
            };

            return Ok(Hasil);
        }

        #endregion
    }
}
