﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.Entity;
using System.Drawing;
using System.Linq.Dynamic.Core;
using WebAPI_NETCore.Connection;
using WebAPI_NETCore.Models;
using static System.Runtime.InteropServices.JavaScript.JSType;
using EntityState = Microsoft.EntityFrameworkCore.EntityState;

namespace WebAPI_NETCore.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MasterEmployeeController : ControllerBase
    {

        #region Property Connection Database
        private readonly ApplicationDBContext DBContext;
        private readonly ILogger<MasterEmployeeController> Logger;

        public MasterEmployeeController(ILogger<MasterEmployeeController> logger, ApplicationDBContext dbcontext)
        {
            Logger = logger;
            DBContext = dbcontext;
        }
        #endregion

        /// <summary>
        /// MasterEmployee API CRUD : Create, Read, Update, Delete --------------------------------------------------------------------------------------------//
        /// </summary>

        [HttpGet, Produces("application/json")]
        public IActionResult GetAllDataMasterEmployee()
        {
            List<MasterEmployee> GetData_WithList = DBContext.MasterEmployees.OrderBy(x => x.Id).ToList();

            return Ok(GetData_WithList);
        }

        [HttpGet, Produces("application/json")]
        public IActionResult GetAllDataMasterEmployeeAsQueryable()
        {
            var GetData_WithAsQueryable = DBContext.MasterEmployees.OrderBy(x => x.Id).AsQueryable();
            int Hitung = GetData_WithAsQueryable.Count();

            #region Example Get Data with LINQ and use condition if else on the property model
            //var GetData = DBContext.MasterEmployees.Select(Hasil => new MasterEmployee
            //{
            //    Id = Hasil.Id,
            //    KodeMenu = Hasil.KodeMenu,
            //    NamaMenu = Hasil.NamaMenu,
            //    JenisMenu = Hasil.JenisMenu == "Food" ? "Food" : "Drink",
            //    HargaSatuan = Hasil.HargaSatuan
            //}).Where(x => x.Id == 16).Single();
            ///*
            //--> 
            //--> Tanda Baca ? adalah If
            //--> Tanda Baca : adalah Else 
            //--> Hasil.JenisMenu == "Food" saja dengan GetData.JenisMenu == JenisFood
            // */
            //var JenisFood = "Food";
            //var JenisDrink = "Drink";
            //var Jenis = string.Empty;
            //if (GetData.JenisMenu == JenisFood || GetData.JenisMenu.Contains(JenisFood))
            //{
            //    Jenis = JenisFood;
            //}
            //else
            //{
            //    Jenis = JenisDrink;
            //}
            #endregion

            return Ok(GetData_WithAsQueryable);
        }

        [HttpPost, Produces("application/json")]
        public IActionResult AddData(MasterEmployee Payload)
        {
            #region Membuat atau menyiapkan penampung dalam bentuk single temporary dengan object MasterEmployee, and call the database connection for added the data, kemudian menampilkan hasil data yang sudah diupdate kedalam return.
            var TemporaryDataInsert = new MasterEmployee()
            {
                Nik = Payload.Nik,
                NamaLengkap = Payload.NamaLengkap,
                NamaPanggilan = Payload.NamaPanggilan,
                TempatLahir = Payload.TempatLahir,
                TanggalLahir = Payload.TanggalLahir,
                JenisKelamin = Payload.JenisKelamin,
                AlamatLengkap = Payload.AlamatLengkap,
                AlamatDomisili = Payload.AlamatDomisili,
                NomerTelepon = Payload.NomerTelepon,
                Email1 = Payload.Email1,
                Email2 = Payload.Email2,
                Jabatan = Payload.Jabatan,
                UsernameLogin = Payload.UsernameLogin,
                PasswordLogin = Payload.PasswordLogin,
                HakAkses = Payload.HakAkses,
                IsActive = Payload.IsActive
            };
            #endregion

            #region Call the database connection and then insert data.
            DBContext.Add(TemporaryDataInsert);
            DBContext.SaveChanges();
            #endregion

            return Ok(TemporaryDataInsert);
        }

        [HttpPost, Produces("application/json")]
        public IActionResult AddDataMultiple(List<MasterEmployee> Payload)
        {
            #region Membuat atau menyiapkan penampung data lebih dari satu menggunakan list dan perulangan foreach, and call the database connection for added multiple the data, kemudian menampilkan hasil data yang sudah ditambah kedalam return.
            var List_Data = new List<dynamic>();
            var TemporaryDataInsert = new List<MasterEmployee>();
            var DBContext = new ApplicationDBContext();
            foreach (var x in Payload)
            {
                TemporaryDataInsert.Add(new MasterEmployee
                {
                    Nik = x.Nik,
                    NamaLengkap = x.NamaLengkap,
                    NamaPanggilan = x.NamaPanggilan,
                    TempatLahir = x.TempatLahir,
                    TanggalLahir = x.TanggalLahir,
                    JenisKelamin = x.JenisKelamin,
                    AlamatLengkap = x.AlamatLengkap,
                    AlamatDomisili = x.AlamatDomisili,
                    NomerTelepon = x.NomerTelepon,
                    Email1 = x.Email1,
                    Email2 = x.Email2,
                    Jabatan = x.Jabatan,
                    UsernameLogin = x.UsernameLogin,
                    PasswordLogin = x.PasswordLogin,
                    HakAkses = x.HakAkses,
                    IsActive = x.IsActive
                });
            }

            List_Data.AddRange(TemporaryDataInsert);
            #endregion

            #region Call the database connection and then insert multiple data.
            DBContext.MasterEmployees.AddRange(TemporaryDataInsert);
            DBContext.SaveChanges();
            #endregion

            return Ok(List_Data);
        }

        [HttpPost, Produces("application/json")]
        public IActionResult UpdateData(MasterEmployee Payload)
        {
            #region Example 1 With List Data

            #region Mengambil data dari table dalam bentuk AsQueryable / ToList, dimana property where yang digunakan sebagai pencarian yaitu Nik
            //var GetData = DBContext.MasterEmployees.Where(x => x.Nik == Payload.Nik).AsQueryable();
            #endregion

            #region Membuat perulangan dalam bentuk foreach yang digunakan untuk melakukan update data, dari data yang sudah di'ditemukan atau sudah diambil dari table, kemudian menampilkan hasil data yang sudah diupdate kedalam return.
            //foreach (var x in GetData)
            //{
            //    if (GetData != null || GetData.Count() > 0) // <-- Checking if the data already exists
            //    {
            //        x.Nik = Payload.Nik;
            //        x.NamaLengkap = Payload.NamaLengkap;
            //        x.NamaPanggilan = Payload.NamaPanggilan;
            //        x.TempatLahir = Payload.TempatLahir;
            //        x.TanggalLahir = Payload.TanggalLahir;
            //        x.JenisKelamin = Payload.JenisKelamin;
            //        x.AlamatLengkap = Payload.AlamatLengkap;
            //        x.AlamatDomisili = Payload.AlamatDomisili;
            //        x.NomerTelepon = Payload.NomerTelepon;
            //        x.Email1 = Payload.Email1;
            //        x.Email2 = Payload.Email2;
            //        x.Jabatan = Payload.Jabatan;
            //        x.UsernameLogin = Payload.UsernameLogin;
            //        x.PasswordLogin = Payload.PasswordLogin;
            //        x.HakAkses = Payload.HakAkses;
            //        x.IsActive = Payload.IsActive;
            //        DBContext.SaveChanges();
            //    }
            //}
            //return Ok(Payload);
            #endregion

            #endregion

            #region Example 2 With Single Data 

            #region Mengambil data hanya satu menggunakan FirstOrDefault, dimana pada FirstOrDefault property yang digunakan sebagai pencarian yaitu Nik
            var GetData = DBContext.MasterEmployees.FirstOrDefault(x => x.Nik == Payload.Nik);
            //var GetData = DBContext.MasterEmployees.Where(x => x.Nik == Payload.MasterEmployees).FirstOrDefault();
            #endregion

            #region Checking if the data already exists and call the database connection for modify the data, kemudian menampilkan hasil data yang sudah diupdate kedalam return.
            if (GetData != null || GetData.Nik != null || GetData.Nik == Payload.Nik)
            {
                GetData.Nik = Payload.Nik;
                GetData.NamaLengkap = Payload.NamaLengkap;
                GetData.NamaPanggilan = Payload.NamaPanggilan;
                GetData.TempatLahir = Payload.TempatLahir;
                GetData.TanggalLahir = Payload.TanggalLahir;
                GetData.JenisKelamin = Payload.JenisKelamin;
                GetData.AlamatLengkap = Payload.AlamatLengkap;
                GetData.AlamatDomisili = Payload.AlamatDomisili;
                GetData.NomerTelepon = Payload.NomerTelepon;
                GetData.Email1 = Payload.Email1;
                GetData.Email2 = Payload.Email2;
                GetData.Jabatan = Payload.Jabatan;
                GetData.UsernameLogin = Payload.UsernameLogin;
                GetData.PasswordLogin = Payload.PasswordLogin;
                GetData.HakAkses = Payload.HakAkses;
                GetData.IsActive = Payload.IsActive;
                DBContext.SaveChanges();
            }
            #endregion

            return Ok(Payload);

            #endregion
        }

        [HttpPatch, Produces("application/json")]
        public IActionResult UpdateDataMultiple(List<MasterEmployee> Payload)
        {
            #region Membuat atau menyiapkan penampung dalam bentuk list dynamic
            var ListData_After = new List<MasterEmployee>();
            #endregion

            #region Mengambil data dari table dengan kondisi where dengan payload yang dikirim, untuk property where yang digunakan sebagai pencarian yaitu NIK. Kemudian data yang sudah diambil akan dan akan disimpan kedalam penampung berbentuk List.
            var GetData_Before = DBContext.MasterEmployees.Where(p => Payload.Select(x => x.Nik).Contains(p.Nik)).Select(Hasil => new MasterEmployee
            {
                Id = Hasil.Id,
                Nik = Hasil.Nik,
                NamaLengkap = Hasil.NamaLengkap,
                NamaPanggilan = Hasil.NamaPanggilan,
                TempatLahir = Hasil.TempatLahir,
                TanggalLahir = Hasil.TanggalLahir,
                JenisKelamin = Hasil.JenisKelamin,
                AlamatLengkap = Hasil.AlamatLengkap,
                AlamatDomisili = Hasil.AlamatDomisili,
                NomerTelepon = Hasil.NomerTelepon,
                Email1 = Hasil.Email1,
                Email2 = Hasil.Email2,
                Jabatan = Hasil.Jabatan,
                UsernameLogin = Hasil.UsernameLogin,
                PasswordLogin = Hasil.PasswordLogin,
                HakAkses = Hasil.HakAkses,
                IsActive = Hasil.IsActive,
            }).ToList();
            #endregion

            #region Membuat perulangan dalam bentuk foreach yang digunakan untuk melakukan update data, dari data yang sudah di'ditemukan atau sudah diambil dari table.
            foreach (var Obj in GetData_Before)
            {
                #region Object A : Membuat object dalam bentuk variable sesuai dengan kolom yang ada
                var Id = GetData_Before.Where(x => x.Nik == Obj.Nik).FirstOrDefault().Id;
                var NIK = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.Nik;
                var NamaLengkap = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.NamaLengkap;
                var NamaPanggilan = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.NamaPanggilan;
                var TempatLahir = Payload?.Where(x => x.Nik == Obj.Nik).FirstOrDefault()?.TempatLahir;
                var TanggalLahir = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.TanggalLahir;
                var JenisKelamin = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.JenisKelamin;
                var AlamatLengkap = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.AlamatLengkap;
                var AlamatDomisili = Payload?.Where(x => x.Nik == Obj.Nik).FirstOrDefault()?.AlamatDomisili;
                var NomerTelepon = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.NomerTelepon;
                var Email1 = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.Email1;
                var Email2 = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.Email2;
                var Jabatan = Payload?.Where(x => x.Nik == Obj.Nik).FirstOrDefault()?.Jabatan;
                var UsernameLogin = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.UsernameLogin;
                var PasswordLogin = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.PasswordLogin;
                var HakAkses = Payload?.Where(x => x?.Nik == Obj?.Nik).FirstOrDefault()?.HakAkses;
                var IsActive = Payload?.Where(x => x.Nik == Obj.Nik).FirstOrDefault()?.IsActive;
                #endregion

                #region Object B : Membuat object tampung dari object A dalam bentuk variable dan akan disimpan kedalam penampung berbentuk List untuk object B.
                var TemporaryDataAfter = new MasterEmployee
                {
                    Id = Id,
                    Nik = NIK,
                    NamaLengkap = NamaLengkap,
                    NamaPanggilan = NamaPanggilan,
                    TempatLahir = TempatLahir,
                    TanggalLahir = TanggalLahir,
                    JenisKelamin = JenisKelamin,
                    AlamatLengkap = AlamatLengkap,
                    AlamatDomisili = AlamatDomisili,
                    NomerTelepon = NomerTelepon,
                    Email1 = Email1,
                    Email2 = Email2,
                    Jabatan = Jabatan,
                    UsernameLogin = UsernameLogin,
                    PasswordLogin = PasswordLogin,
                    HakAkses = HakAkses,
                    IsActive = IsActive,
                };
                ListData_After.Add(TemporaryDataAfter);
                #endregion

                #region Call the database connection and then modify the data.
                DBContext.Update(TemporaryDataAfter).Property(x => x.Id).IsModified = false;
                DBContext.SaveChanges();
                #endregion
            }
            #endregion

            #region Membuat object dalam bentuk variable dynamic, untuk menampilkan hasil data yang sudah diupdate kedalam return.
            dynamic Result = new
            {
                GetData_Before,
                ListData_After
            };
            #endregion

            return Ok(Result);
        }

        [HttpGet("{NIK}"), Produces("application/json")]
        public IActionResult GetDataByNik(long NIK)
        {
            #region Example : Get One Data Or Get Single Data
            var GetData = DBContext.MasterEmployees.FirstOrDefault(x => x.Nik == NIK);
            object Result = new
            {
                GetData
            };
            #endregion

            return Ok(GetData);
        }

        [HttpGet, Produces("application/json")]
        public IActionResult GetDataMultipleInt(List<int> Payload)
        {
            var ListData = new List<dynamic>();
            var StringJoin = string.Join(",", Payload);
            var SplitData = StringJoin.Split(",");
            foreach (var Obj in SplitData)
            {
                var GetData = DBContext.MasterEmployees.Where(x => x.Id == Convert.ToInt16(Obj)).SingleOrDefault();
                ListData.Add(GetData);
            }
            return Ok(ListData);
        }

        [HttpGet, Produces("application/json")]
        public IActionResult GetDataMultipleString(List<string> Payload)
        {
            var ListData = new List<dynamic>();
            var StringJoin = string.Join(",", Payload);
            var SplitData = StringJoin.Split(",");
            foreach (var Obj in SplitData)
            {
                var GetData = DBContext.MasterEmployees.Where(x => x.Nik == Convert.ToInt64(Obj)).Single(); // pake convert ToString
                ListData.Add(GetData);
            }
            return Ok(ListData);
        }

        [HttpDelete, Produces("application/json")]
        public IActionResult DeleteDataMultipleByNIK(List<long> NIK)
        {
            #region Example : Delete More Than One (Delete Multiple Data)
            var TemporaryData = new MasterEmployee();
            var ListDataDelete = new List<MasterEmployee>();
            var StringJoin = string.Join(",", NIK.ToList());
            var SplitData = StringJoin.Split(",");
            foreach (var obj in SplitData)
            {
                var GetData = (from x in DBContext.MasterEmployees.Where(x => x.Nik == Convert.ToInt64(obj))
                               select new MasterEmployee
                               {
                                   Id = x.Id,
                                   Nik = x.Nik,
                                   NamaLengkap = x.NamaLengkap,
                                   NamaPanggilan = x.NamaPanggilan,
                                   TempatLahir = x.TempatLahir,
                                   TanggalLahir = x.TanggalLahir,
                                   JenisKelamin = x.JenisKelamin,
                                   AlamatLengkap = x.AlamatLengkap,
                                   AlamatDomisili = x.AlamatDomisili,
                                   NomerTelepon = x.NomerTelepon,
                                   Email1 = x.Email1,
                                   Email2 = x.Email2,
                                   Jabatan = x.Jabatan,
                                   UsernameLogin = x.UsernameLogin,
                                   PasswordLogin = x.PasswordLogin,
                                   HakAkses = x.HakAkses,
                                   IsActive = x.IsActive,
                               }).AsQueryable();

                foreach (var x in GetData)
                {
                    TemporaryData.Id = x.Id;
                    TemporaryData.Nik = x.Nik;
                    TemporaryData.NamaLengkap = x.NamaLengkap;
                    TemporaryData.NamaPanggilan = x.NamaPanggilan;
                    TemporaryData.TempatLahir = x.TempatLahir;
                    TemporaryData.TanggalLahir = x.TanggalLahir;
                    TemporaryData.JenisKelamin = x.JenisKelamin;
                    TemporaryData.AlamatLengkap = x.AlamatLengkap;
                    TemporaryData.AlamatDomisili = x.AlamatDomisili;
                    TemporaryData.NomerTelepon = x.NomerTelepon;
                    TemporaryData.Email1 = x.Email1;
                    TemporaryData.Email2 = x.Email2;
                    TemporaryData.Jabatan = x.Jabatan;
                    TemporaryData.UsernameLogin = x.UsernameLogin;
                    TemporaryData.PasswordLogin = x.PasswordLogin;
                    TemporaryData.HakAkses = x.HakAkses;
                    TemporaryData.IsActive = x.IsActive;
                    ListDataDelete.Add(TemporaryData);

                    DBContext.MasterEmployees.Entry(TemporaryData).State = EntityState.Deleted;
                    DBContext.SaveChanges();
                }
            }
            #endregion

            return Ok(ListDataDelete);
        }

        [HttpDelete("{NIK}"), Produces("application/json")]
        public IActionResult DeleteDatabyNIK(long NIK)
        {
            #region Example : Delete One Data Or Delete Single Data
            var GetData = DBContext.MasterEmployees.Where(x => x.Nik == NIK).FirstOrDefault();
            DBContext.MasterEmployees.Entry(GetData).State = EntityState.Deleted;
            DBContext.SaveChanges();
            object Result = new
            {
                GetData
            };
            #endregion

            return Ok(GetData);

        }

    }
}
