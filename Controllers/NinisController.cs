﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI_NETCore.Models;

namespace WebAPI_NETCore.Controllers
{
    [Route("API/[controller]/[action]")]
    [ApiController]
    public class NinisController : ControllerBase
    {
        #region Data

        IList<Employee> employees = new List<Employee>()
        {
            new Employee()
            {
                EmployeeId = 1, EmployeeName = "Mochammad Reza Fahlevi", Address = "Cilegon", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 2, EmployeeName = "Mario Kevin Binuan", Address = "Bengkulu", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 3, EmployeeName = "Ardelingga Ngopi Masseeeh", Address = "Cirebon", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 4, EmployeeName = "Fadil Kudil", Address = "Tangerang", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 5, EmployeeName = "Badrul Salim", Address = "Pandeglang", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 6, EmployeeName = "Abdul Kirom Wong Tegal", Address = "Tegal", Department = "Support"
            },
            new Employee()
            {
                EmployeeId = 7, EmployeeName = "Arren Rediman", Address = "Medan", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 8, EmployeeName = "Virda Jeeehh", Address = "Indramayu", Department = "KP"
            },
            new Employee()
            {
                EmployeeId = 9, EmployeeName = "Ayu Ayunan", Address = "Indramayu", Department = "KP"
            },
            new Employee()
            {
                EmployeeId = 10, EmployeeName = "Dinda Denda", Address = "Indramayu", Department = "KP"
            },
            new Employee()
            {
                EmployeeId = 11, EmployeeName = "Ninis Mangga Harum Manis", Address = "Indramayu", Department = "KP"
            },
        };

        #endregion


        #region API Ninis

        [HttpGet, Produces("application/json")]
        public IActionResult Quiz()
        {
            /* Quiz
            Cari wording dari EmployeeName, Address, dan Department, 
            lalu gabungkan kata tersebut sehingga menjadi kalimat "Mangga Harum Manis Merupakan Ciri Khas Kota Indramayu, Sebuah Bingkisan Dari Ninis Tim KP"..
            kemudian tampilkan hasil response tersebut pada postman ?

            * Library .NET : "Where", "Subtring", "Concat", dan atau "String.Join"
            * References : 
            - https://linqexamples.com/projections/select.html#query-syntax-2
            - https://www.tutorialsteacher.com/linq/linq-query-syntax
            - https://dotnettutorials.net/lesson/where-filtering-operators-in-linq/?expand_article=1
            - https://www.geeksforgeeks.org/linq-filtering-operator-where/
            - https://www.c-sharpcorner.com/UploadFile/mahesh/substring-in-C-Sharp/
            - https://www.geeksforgeeks.org/c-sharp-substring-method/
            - https://www.dotnetperls.com/substring
            - https://www.w3schools.com/cs/cs_strings_concat.php / https://www.w3schools.com/cs/trycs.php?filename=demo_strings_concat
            - https://www.geeksforgeeks.org/c-sharp-string-concat-with-examples-set-2/
            - https://www.codingninjas.com/studio/library/csharp-concatenate-strings
            - https://www.geeksforgeeks.org/c-sharp-join-method-set-1/
            - https://www.dotnetperls.com/string-join
            - https://www.programiz.com/csharp-programming/library/string/join
            - https://www.javatpoint.com/csharp-string-join
            - https://www.tutorialspoint.com/chash-join-method
            - https://www.tutlane.com/tutorial/csharp/csharp-string-join-method
            - https://www.csharptutorial.net/csharp-string-methods/csharp-string-join/
            - https://zetcode.com/csharp/joinstring/#google_vignette
            */

            #region Please enter the coding answer below
            var GetNama = employees.Where(x => x.EmployeeName.Contains("Ninis")).FirstOrDefault();
            var Nama = GetNama.EmployeeName.Substring(0, 5);

            var GetMangga = employees.Where(x => x.EmployeeName.Contains("Mangga")).FirstOrDefault();
            var Mangga = GetNama.EmployeeName.Substring(0, 10);

            //var Hasil adalah sebuahvariable penampung untuk ditampilkan diakhir postman
            var Hasil = string.Concat(Nama, Mangga); //Nama + Mangga;
            #endregion

            return Ok(Hasil);
        }

        #endregion
    }
}
