﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI_NETCore.Connection;
using WebAPI_NETCore.Models;

namespace WebAPI_NETCore.Controllers
{
    [Route("API/[controller]/[action]")]
    [ApiController]
    public class LatihanController : ControllerBase
    {
        #region Data Employee Yang Belum Dibungkus Dengan Action / Method

        IList<Employee> employees = new List<Employee>()
        {
            new Employee()
            {
                EmployeeId = 1, EmployeeName = "Mochammad Reza Fahlevi", Address = "Cilegon", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 2, EmployeeName = "Mario Kevin Binuan", Address = "Bengkulu", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 3, EmployeeName = "Ardelingga Ngopi Masseeeh", Address = "Cirebon", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 4, EmployeeName = "Fadil Kudil", Address = "Tangerang", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 5, EmployeeName = "Badrul Salim", Address = "Pandeglang", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 6, EmployeeName = "Abdul Kirom Wong Tegal", Address = "Tegal", Department = "Support"
            },
            new Employee()
            {
                EmployeeId = 7, EmployeeName = "Arren Rediman", Address = "Medan", Department = "IT"
            },
            new Employee()
            {
                EmployeeId = 8, EmployeeName = "Virda Jeeehh", Address = "Indramayu", Department = "KP"
            },
            new Employee()
            {
                EmployeeId = 9, EmployeeName = "Ayu Ayunan", Address = "Indramayu", Department = "KP"
            },
            new Employee()
            {
                EmployeeId = 10, EmployeeName = "Dinda Denda", Address = "Indramayu", Department = "KP"
            },
            new Employee()
            {
                EmployeeId = 11, EmployeeName = "Ninis Mangga Harum Manis", Address = "Indramayu", Department = "KP"
            },
        };

        #endregion


        #region API Dinda

        [HttpGet, Produces("application/json")]
        public IActionResult Dinda()
        {
            /* Quiz : 
            Ambil data yang memiliki Department "KP", 
            lalu jumlahkan berapa banyak orang yang berada di'department KP..
            kemudian tampilkan hasil response tersebut pada postman ?

            * Library .NET : "Where" dan "Sum"
            * References : 
            - https://linqexamples.com/projections/select.html#query-syntax-2
            - https://www.tutorialsteacher.com/linq/linq-query-syntax
            - https://dotnettutorials.net/lesson/where-filtering-operators-in-linq/?expand_article=1
            - https://www.geeksforgeeks.org/linq-filtering-operator-where/
            - https://www.csharp-examples.net/linq-sum/
            - https://dotnettutorials.net/lesson/linq-sum-method/
            - https://www.csharptutorial.net/csharp-linq/linq-sum/
            */

            #region Please enter the coding answer below

            #endregion

            return Ok();
        }

        #endregion


        #region API Virda

        [HttpGet, Produces("application/json")]
        public IActionResult Virda()
        {
            /* Quiz : 
            Ambil data yang memiliki Department "KP dan IT", 
            lalu ambil data 2 dari masing-masing Department..
            kemudian tampilkan hasil response tersebut pada postman ?

            * Library .NET : "Where" dan "Take"
            * References : 
            - https://linqexamples.com/projections/select.html#query-syntax-2
            - https://www.tutorialsteacher.com/linq/linq-query-syntax
            - https://dotnettutorials.net/lesson/where-filtering-operators-in-linq/?expand_article=1
            - https://www.geeksforgeeks.org/linq-filtering-operator-where/
            - https://linuxhint.com/csharp-linq-take/
            - https://linqexamples.com/partitioning/take.html
            - https://stackoverflow.com/questions/15385905/linq-with-skip-and-take
            - https://www.csharptutorial.net/csharp-linq/linq-take/
            */

            #region Please enter the coding answer below

            #endregion

            return Ok();
        }

        #endregion


        #region API Ayu

        [HttpGet, Produces("application/json")]
        public IActionResult Ayu()
        {
            /* Quiz
            Cari data yang memiliki Address "Indramayu", 
            lalu grouping semua data berdasarkan Address..
            kemudian tampilkan EmployeeName dan Address, sehingga akan menampilkan hasil "4 nama dari EmployeeName dan 1 nama dari Address"

            * Ouput yang dihasilkan seperti dibawah ini :
            - EmployeeName : Virda Jeeehh
            - EmployeeName : Ayu Ayunan
            - EmployeeName : Dinda Denda
            - EmployeeName : Ninis Mangga Harum Manis
            - Address      : Indramayu

            kemudian tampilkan hasil response tersebut pada postman ?

            * Library .NET : "Where" dan "GroupBy"
            * References : 
            - https://linqexamples.com/projections/select.html#query-syntax-2
            - https://www.tutorialsteacher.com/linq/linq-query-syntax
            - https://dotnettutorials.net/lesson/where-filtering-operators-in-linq/?expand_article=1
            - https://www.geeksforgeeks.org/linq-filtering-operator-where/
            - https://stackoverflow.com/questions/7325278/group-by-in-linq
            - https://www.csharptutorial.net/csharp-linq/linq-groupby/
            - https://www.tutorialsteacher.com/linq/linq-grouping-operator-groupby-tolookup
            - https://www.javatpoint.com/linq-groupby-method
            */

            #region Please enter the coding answer below

            #endregion

            return Ok();
        }

        #endregion


        #region API Ninis

        [HttpGet, Produces("application/json")]
        public IActionResult Ninis()
        {
            /* Quiz
            Cari wording dari EmployeeName, Address, dan Department, 
            lalu gabungkan kata tersebut sehingga menjadi kalimat "Mangga Harus Manis Merupakan Ciri Khas Kota Indramayu, Merupakan Sebuah Bingkisan Dari Ninis Tim KP"..
            kemudian tampilkan hasil response tersebut pada postman ?

            * Library .NET : "Where", "Subtring", "Concat", dan atau "String.Join"
            * References : 
            - https://linqexamples.com/projections/select.html#query-syntax-2
            - https://www.tutorialsteacher.com/linq/linq-query-syntax
            - https://dotnettutorials.net/lesson/where-filtering-operators-in-linq/?expand_article=1
            - https://www.geeksforgeeks.org/linq-filtering-operator-where/
            - https://www.c-sharpcorner.com/UploadFile/mahesh/substring-in-C-Sharp/
            - https://www.geeksforgeeks.org/c-sharp-substring-method/
            - https://www.dotnetperls.com/substring
            - https://www.w3schools.com/cs/cs_strings_concat.php / https://www.w3schools.com/cs/trycs.php?filename=demo_strings_concat
            - https://www.geeksforgeeks.org/c-sharp-string-concat-with-examples-set-2/
            - https://www.codingninjas.com/studio/library/csharp-concatenate-strings
            - https://www.geeksforgeeks.org/c-sharp-join-method-set-1/
            - https://www.dotnetperls.com/string-join
            - https://www.programiz.com/csharp-programming/library/string/join
            - https://www.javatpoint.com/csharp-string-join
            - https://www.tutorialspoint.com/chash-join-method
            - https://www.tutlane.com/tutorial/csharp/csharp-string-join-method
            - https://www.csharptutorial.net/csharp-string-methods/csharp-string-join/
            - https://zetcode.com/csharp/joinstring/#google_vignette
            */

            #region Please enter the coding answer below

            #endregion

            return Ok();
        }

        #endregion
    }
}
